@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="btn-group" role="group" aria-label="Blog Management">
                            <a href="{{ route('news.create') }}" class="btn btn-primary">Create News</a>
                            <a href="{{ route('categories.index') }}" class="btn btn-primary">Category</a>

                        </div>
                    <hr>
                    <hr>
                    <p class="text-center">All News</p>
                    @foreach($news as $singleNews)
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="img-responsive" src="{{ asset('/storage/news-images') }}/{{ $singleNews->image }}">
                            </div>
                            <div class="col-sm-9">
                                <h3>{{ $singleNews->title }}</h3>
                                <p>{{ $singleNews->excerpt }}</p>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('news.edit', [ 'id' => $singleNews->id ] ) }}" class="btn btn-primary">Edit</a>
                                <form method="post" action="{{ route('news.destroy', [ 'id' => $singleNews->id ] ) }}" style="display:inline-block">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="submit" class="btn btn-danger" value="delete">
                                </form>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    {{ $news->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
