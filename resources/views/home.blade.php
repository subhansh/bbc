@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="text text-center">BBC</h1>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <h3 class="text-center">Categories</h3>
                @if( count( $categories ) > 0 )
                    <ul class="list-group">
                        <li class="list-group-item"><a href="{{ route('news.index') }}">All news</a></li>
                        @foreach( $categories as $category )
                            <li class="list-group-item"><a href="{{ route('listByCat', [ 'categoryName' => $category->title ] ) }}">{{ $category->title }}</a></li>
                            @if( $category->has('subcategories') )
                                <ul>
                                    @foreach( $category->subcategories as $subcategory )
                                        <li class="list-group-item"><a href="{{ route('listByCat', [ 'categoryName' => $subcategory->title ] ) }}">{{ $subcategory->title }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="col-sm-4" style="border:solid black;">
            right panel
                <br><br>

            </div>

        </div>
    </div>
@endsection
