@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="text text-center">BBC</h1>
        </div>
        <div class="row">
            @foreach( $instaData as $singleData )
                <div class="col-sm-4" style="border:solid #000">
                    <h3><strong>{{ $singleData->caption->text }}</strong></h3>
                    <a href="{{ $singleData->link }}">
                        <img width="{{ $singleData->images->thumbnail->width }}" height="{{ $singleData->images->thumbnail->height }}"
                             class="img-responsive img-rounded" src="{{ $singleData->images->thumbnail->url }}" alt="">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
