@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create news</div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('news.update', [ 'id' => $news->id ] ) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $news->title }}" >
                            </div>
                            <div class="form-group">
                                <label for="body">News body</label>
                                <textarea rows="10" class="form-control" id="body" name="body">{{ $news->body }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="excerpt">Excerpt</label>
                                <textarea rows="3" class="form-control" id="excerpt" name="excerpt">{{ $news->excerpt }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="type">Type</label>
                                <select name="type" id="type" class="form-control">
                                    <option {{ $news->type == 'آموزشی' ? 'selected' : '' }}>آموزشی</option>
                                    <option {{ $news->type == 'اجتماعی' ? 'selected' : '' }}>اجتماعی</option>
                                    <option {{ $news->type == 'فرهنگی' ? 'selected' : '' }}>فرهنگی</option>
                                    <option {{ $news->type == 'ورزشی' ? 'selected' : '' }}>ورزشی</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kind">Dedicated</label>
                                <input type="radio" value="ویژه" name="kind" {{ $news->kind == 'ویژه' ? 'checked' : '' }} >
                            </div>
                            <div class="form-group">
                                <label for="kind">No Dedicated</label>
                                <input type="radio" value="عادی" name="kind" {{ $news->kind != 'ویژه' ? 'checked' : '' }} >
                            </div>
                            <div class="form-group" style="border: 1px solid #6f6;">
                                <div>
                                    <label for="imageInput">Image</label>
                                    <img id="newsImage" src="{{ asset('/storage/news-images') }}/{{ $news->image }}" class="img-responsive" width="200" height="180">
                                </div>
                                <input type="file" class="form-control" id="imageInput" name="image" >
                            </div>
                            <div class="form-group">
                                <label for="state">Active</label>
                                <input type="checkbox" id="state" name="state" data-toggle="toggle" data-onstyle="primary"
                                        {{ $news->is_active ? 'checked' : '' }}>
                            </div>

                            <div class="form-group">
                                @foreach( $categories as $category )
                                    <label for="categories">{{ $category->title }}</label>
                                    <input type="checkbox" name="categories[]" value="{{ $category->id }}"  data-toggle="toggle"
                                           data-onstyle="primary" {{ in_array( $category->id, $newsCategoriesId ) ? 'checked' : '' }} >
                                @endforeach
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
