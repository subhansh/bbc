@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
             <h1>{{ $news->title }}</h1>
        </div>
        <img class="img-thumbnail" src="{{ asset('/storage/news-images') }}/{{ $news->image }}" alt="">
        <div>
            {!! $news->body !!}
        </div>
        <br>
        <p class="text-right bg-info text-white"><strong>{{ $news->type }}</strong></p>
        <hr>
        <h3 class="text text-xl-left">Comments</h3>
        <div>
            @foreach( $comments as $comment )
                <div style="border-bottom: solid 2px #000">
                    <h3><strong>{{ $comment->name }}</strong></h3>
                    <p style="padding-left: 30px;">
                        {{ $comment->description }}
                    </p>
                    @if( !$comment->is_active )
                        <div class="pull-right">
                            <a href="{{ route('comments.approve', [ 'id' => $comment->id ]) }}" class="btn btn-success">Approve</a>
                        </div><div class="clearfix"></div>
                    @endif
                </div>
            @endforeach
        </div><br>
        <div>
            <p class="text text-center">Leave a comment</p>
            <br>
            <form method="post" action="{{ route('comments.store') }}">
                {{ csrf_field() }}
                <input type="hidden" name="news_id" value="{{ $news->id }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" value="{{ old('name') }}" id="name" name="name" >
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" value="{{ old('email') }}" id="email" name="email" >
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea type="text" class="form-control" id="description" name="description" >{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" value="Register" type="submit" class="form-control">
                </div>
            </form>
        </div>
    </div>
@endsection
