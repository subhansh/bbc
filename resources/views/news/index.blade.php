@extends('layouts.app')

@section('content')
    <div class="container">
        @if( count($news) > 0 )
            @foreach( $news as $singleNews )
                <div class="row">
                    <div class="col-sm-3">
                    <img class="img-responsive img-thumbnail" src="{{ asset('/storage/news-images') }}/{{ $singleNews->image }}" alt="">
                    </div>
                    <div class="col-sm-9">
                        <a href="/news/{{ $singleNews->id }}"><h3>{{ $singleNews->title }}</h3></a>
                        <p>{{ $singleNews->excerpt }}</p>
                        <m class="text-white bg-success">{{ $singleNews->kind == 'ویژه' ? 'ویژه' : '' }}</m>
                        @auth
                        <div class="pull-right">
                            <a href="{{ route('news.edit', [ 'id' => $singleNews->id ] ) }}" class="btn btn-primary">Edit</a>
                            <form method="post" action="{{ route('news.destroy', [ 'id' => $singleNews->id ] ) }}" style="display:inline-block">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="submit" class="btn btn-danger" value="delete">
                            </form>
                        </div>
                        @endauth
                    </div>
                </div><br>
            @endforeach
            <div class="row">
                {{ $news->links() }}
            </div>
        @else
            <h3>There isn't any news.</h3>
        @endif

    </div>
@endsection
