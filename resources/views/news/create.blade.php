@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create news</div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('news.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" value="{{ old('title') }}" id="title" name="title" >
                            </div>
                            <div class="form-group">
                                <label for="body">News body</label>
                                <textarea rows="10" class="form-control" id="body" name="body">{{ old('body') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="excerpt">Excerpt</label>
                                <textarea rows="3" class="form-control" id="excerpt" name="excerpt">{{ old('excerpt') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="type">Type</label>

                                <select name="type" id="type" class="form-control">
                                    <option {{ old('type') == 'آموزشی' ? 'selected' : '' }}>آموزشی</option>
                                    <option {{ old('type') == 'اجتماعی' ? 'selected' : '' }}>اجتماعی</option>
                                    <option {{ old('type') == 'فرهنگی' ? 'selected' : '' }}>فرهنگی</option>
                                    <option {{ old('type') == 'ورزشی' ? 'selected' : '' }}>ورزشی</option>
                                </select>
                            </div>
                            <div style="border: 1px solid #6f6">
                                <strong>kind</strong>
                                <div class="form-group">
                                    <label for="kind">Dedicated</label>
                                    <input type="radio" value="ویژه" name="kind" {{ old('kind') == 'ویژه' ? 'checked' : '' }} >
                                </div>
                                <div class="form-group">
                                    <label for="kind">No Dedicated</label>
                                    <input type="radio" value="عادی" name="kind" {{ ( old('kind') == 'عادی' or old('kind') == null ) ? 'checked' : '' }} >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" id="image" name="image" >
                            </div>
                            <div class="form-group">
                                <label for="is_active">Active</label>
                                <input type="checkbox" {{ old('is_active') == "on" ? 'checked' : '' }} id="is_active" name="is_active"  data-toggle="toggle" data-onstyle="primary">
                            </div>
                            <div class="form-group">
                                @foreach( $categories as $category )
                                    <label for="categories">{{ $category->title }}</label>
                                    <input type="checkbox" name="categories[{{ $category->id }}]" value="{{ $category->id }}"  data-toggle="toggle" data-onstyle="primary"
                                           @if(array_key_exists($category->id, old('categories', []))) checked @endif >
                                @endforeach
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
