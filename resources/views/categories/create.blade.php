@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('categories.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="{{ old('title') }}" class="form-control" id="title" name="title" >
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea type="text" class="form-control" id="description" name="description" >{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent</label>

                                <select class="form-control" name="parent_id">
                                    <option value="0">No Parent</option>
                                    @foreach( $categories as $category )
                                        <option value="{{ $category->id }}" {{ $category->id == old('parent_id') ? 'selected' : '' }}>
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
