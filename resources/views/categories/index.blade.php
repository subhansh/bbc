@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>

                    <div class="panel-body">
                        <a class="btn btn-primary" href="{{ route('categories.create') }}">Create new category</a>
                        <hr>
                        <div>
                            @if( count( $categories ) > 0 )
                                <ul>
                                    @foreach( $categories as $category)
                                        <li>
                                            <strong>{{ $category->title }}</strong>
                                            <a href="{{ route('categories.edit', [ 'id' => $category->id ] ) }}" class="btn btn-sm btn-primary">Edit</a>
                                            <form method="post" action="{{ route('categories.destroy', ['id' => $category->id ] ) }}" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </li><br>
                                    @endforeach
                                </ul>
                            @else
                                <p>There isn't any category</p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
