@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('categories.update', [ 'id' => $category->id ] ) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" value="{{ $category->title }}" id="title" name="title" >
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea type="text" class="form-control" id="description" name="description" >{{ $category->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent</label>
                                <select class="form-control" name="parent_id">
                                    <option value="0">No Parent</option>
                                    @foreach( $allCategories as $singleCategory )
                                        <option {{ $category->parent_id == $singleCategory->id ? 'selected' : '' ? 'selected' : '' }}
                                                value="{{ $singleCategory->id }}">{{ $singleCategory->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection