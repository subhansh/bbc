<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('layouts.app', function($view){
            //get all parent categories with their subcategories
            $navCategories = \App\Category::where('parent_id', 0)->with('subcategories')->get();
            //attach the categories to the view.
            $view->with('navCategories', $navCategories);
        });
    }
}
