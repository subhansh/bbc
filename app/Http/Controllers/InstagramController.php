<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InstagramController extends Controller
{
    public function show()
    {
        $response = file_get_contents("https://api.instagram.com/v1/users/self/media/recent/?access_token=6923373226.1677ed0.320995cc2e8d45f3b6ffde0dcf35fc21");
        $response = json_decode($response);
        $instaData = $response->data;
        $instaData = array_slice($instaData,0,9);
//        return $instaData;
        return view('instagram')->with('instaData', $instaData);
    }
}
