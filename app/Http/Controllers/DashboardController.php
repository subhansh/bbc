<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(5);

        return view('dashboard')->with('news', $news);
    }
}
