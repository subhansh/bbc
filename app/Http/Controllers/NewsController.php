<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Category;
use App\Http\Requests\StoreNews;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', [ 'except' => ['index', 'show', 'indexWithCategory', 'indexDedicated'] ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( auth()->check() ) {
            $news = News::orderBy('created_at', 'desc')->paginate(5);
        } else {
            $news = News::orderBy('created_at', 'desc')->where('is_active', 1)->paginate(5);
        }

        return view('news.index')->with( 'news', $news );
    }

    /**
     * Show all news that belongs to this category
     * @param Category Name $category
     * @return view
     */
    public function indexWithCategory($category)
    {
        $currentCollCat = Category::where('title', $category)->first();
        $currentCat = Category::find($currentCollCat->id);
        $subcategories = $currentCat->subcategories;
        $subcategoriesTitle = $this->getSubcategoriesTitle($subcategories);

        $news = News::whereHas('categories', function($query) use ($category, $subcategoriesTitle) {
            $query->where('title', $category)->orWhereIn('title', $subcategoriesTitle);
        })->paginate(5);

        return view('news.index')->with('news', $news);
    }

    /**
     * extract all subcategories title from a subcategories object
     * @param $subcategories
     * @return array subcategories title
     */
    public function getSubcategoriesTitle($subcategories)
    {
        $titles = array();
        foreach ($subcategories as $subcategory){
            array_push($titles, $subcategory->title);
        }
        return $titles;
    }
    /**
     * Show all news that are dedicated
     * @return view
     */
    public function  indexDedicated()
    {
        $news = News::where('kind', 'ویژه')->paginate(5);

        return view('news.index')->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('news.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNews  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $request)
    {
        $news = new News();

        $news->title = $request->input('title');
        $news->body = $request->input('body');
        $news->excerpt = $request->input('excerpt');
        $news->type = $request->input('type');
        $news->kind = $request->input('kind');
        $news->is_active = $request->input('is_active') ? 1 : 0;
        $categories = $request->input('categories');

        $image = $request->file('image');
        if( $image != null ) {
           $imagePath = $request->file('image')->store('public/news-images');
           $imageName = basename($imagePath);
           $news->image = $imageName;
        } else {
            $news->image = "bbc_news_logo.png";
        }

        DB::transaction(function () use ($news, $categories) {
            $news->save();
            $news->categories()->attach( $categories );
        });

        return redirect()->route('dashboard')->with('success', 'News has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);

        if( auth()->check() ) {
            $comments = $news->comments;
        } else {
            $comments = $news->comments()->where('is_active', 1)->get();
        }

        return view( 'news.show' )->with( [ 'news' => $news, 'comments' => $comments ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $categories = Category::all();
        $categoriesId = $this->getCategoriesId( $news->categories );

        return view('news.edit')->with( [ 'news' => $news, 'categories' => $categories,
            'newsCategoriesId' => $categoriesId ] );
    }

    public function getCategoriesId($categories)
    {
        $allCatId = array();
        foreach ($categories as $category) {
            array_push( $allCatId, $category->id);
        }
        return $allCatId;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNews $request, $id)
    {
        $news = News::find($id);

        $news->title = $request->input('title');
        $news->body = $request->input('body');
        $news->excerpt = $request->input('excerpt');
        $news->type = $request->input('type');
        $news->kind = $request->input('kind');
        $news->is_active = $request->input('state') ? 1 : 0;
        $categories = $request->input('categories');

        $image = $request->file('image');
        if( $image != null ) {
            $imagePath = $request->file('image')->store('public/news-images');
            $imageName = basename($imagePath);
            Storage::delete('public/news-images/' . $news->image);
            $news->image = $imageName;
        }

        DB::transaction(function () use($news, $categories) {
            $news->save();

            $news->categories()->detach( Category::all('id') );

            $news->categories()->attach( $categories );
        });

        return redirect()->route('dashboard')->with('success', 'News has been Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        if( $news->delete() ) {
            Storage::delete('public/news-images/' . $news->image);

            return redirect()->route('dashboard' )->with('success', 'news has been deleted.');
        }
    }
}
