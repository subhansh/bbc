<?php

namespace App\Http\Controllers;

use DemeterChain\C;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;
use App\Comment;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [ 'except' => ['store'] ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreComment  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $comment = new Comment();
        $comment->name = $request->input('name');
        $comment->mail = $request->input('email') ? $request->input('email') : '';
        $comment->description = $request->input('description');
        $comment->is_active = 0;
        $comment->news_id = $request->input('news_id');

        if ( $comment->save() ) {
            return redirect()->back()->with('success', 'Comment Created.');
        }
    }

    /**
     * Approve a comment
     * @param $id Comment id
     * @return nothing
     */
    public function approve($id)
    {
        $comment = Comment::find($id);
        $comment->is_active = 1;

        if( $comment->save() ) {
            return redirect()->back()->with('success', 'Comment approved.');
        }
    }
}
