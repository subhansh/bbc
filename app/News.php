<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_news' );
    }
}
