<?php

use Faker\Generator as Faker;
use App\News;

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'body' => $faker->paragraph(8),
        'is_active' => $faker->boolean,
        'image' => 'noimage',
        'kind' => 'dadicated',
        'type' => 'economics',
        'excerpt' => $faker->paragraph(1)
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'mail' => $faker->email,
        'is_active' => $faker->boolean,
        'description' => $faker->paragraph(1),
        'news_id' => function(){
            return News::all()->random();
        }

    ];
});
