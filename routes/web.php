<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('home');
//});

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('/news', 'NewsController');

Route::get('/news/category/{categoryName}', 'NewsController@indexWithCategory')->name('listByCat');

Route::get('/dedicated-news', 'NewsController@indexDedicated')->name('news.dedicated');

Route::resource('/categories', 'CategoriesController');

Route::prefix('comments')->group(function () {

    Route::post('/', 'CommentsController@store')->name('comments.store');

    Route::get('/{id}', 'CommentsController@approve')->name('comments.approve');
});

Route::get('instagram', 'InstagramController@show');

